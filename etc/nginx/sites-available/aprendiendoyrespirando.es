server {
        listen 80;
        listen [::]:80;

        root /var/www/aprendiendoyrespirando.es;
        index index.php;

        server_name  www.aprendiendoyrespirando.es aprendiendoyrespirando.es;
        return       301 https://aprendiendoyrespirando.es$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	root /var/www/aprendiendoyrespirando.es;
	index index.php;
	server_name www.aprendiendoyrespirando.es aprendiendoyrespirando.es;

	ssl_certificate /etc/letsencrypt/live/aprendiendoyrespirando.es/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/aprendiendoyrespirando.es/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
        ssl_dhparam /etc/ssl/certs/dhparam.pem;

	fastcgi_hide_header 'X-Powered-By';
	access_log /var/log/nginx/aprendiendoyrespirando.es.com.access.log;
	error_log /var/log/nginx/aprendiendoyrespirando.es.error.log;

	location = /favicon.ico {
                log_not_found off;
                access_log off;
        }

        location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
        }

	location ~ /(\.|configuration.php|readme.html|license.txt) {
		deny all;
	}

	location / {
		try_files $uri $uri/ /index.php?$args;
		auth_basic "Restricted Content";
		auth_basic_user_file /etc/nginx/.htpasswd;
	}

        location ~ \.php$ {
                include fastcgi_params;
		fastcgi_pass unix:/run/php/php7.1-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

                fastcgi_read_timeout 360s;
                fastcgi_buffer_size 128k;
                fastcgi_buffers 4 256k;
        }

        location ~* .(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
                expires max;
                log_not_found off;
                access_log off;
        }

	location ~ /\.ht {
		deny all;
	}

        location ~ /.well-known {
            allow all;
        }

        location ^~ /.well-known/acme-challenge/ {
                allow all;
        }

        add_header X-Cache $upstream_cache_status;

        #Only permision to GET, HEAD and POST
        if ($request_method !~ ^(GET|HEAD|POST)$) {
                return 444;
        }

        location ~* (\.jpg|\.png|\.css)$ {
                if ($http_referer !~ ^(https://aprendiendoyrespirando.es) ) {
                        return 500;
                }
        }
}
