#fastcgi_cache_path /etc/nginx/cache-laveganademadrid levels=1:2 keys_zone=LAVEGANADEMADRID:100m inactive=60m;

server {
	listen 80;
	listen [::]:80;

	server_name  laveganademadrid.es www.laveganademadrid.es;
	return       301 https://www.laveganademadrid.es$request_uri;
}

server {
        listen 443 http2;
        listen [::]:443 http2;

	ssl_certificate /etc/letsencrypt/live/laveganademadrid.es/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/laveganademadrid.es/privkey.pem; # managed by Certbot
	include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

        
	root /var/www/laveganademadrid.es;
	index index.php index.html index.htm index.nginx-debian.html;
	server_name www.laveganademadrid.es laveganademadrid.es;

	access_log /var/log/nginx/laveganademadrid.es.access.log;
	error_log /var/log/nginx/laveganademadrid.es.error.log;

	fastcgi_hide_header 'X-Powered-By';

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}

	location ~ /(\.|wp-config.php|readme.html|license.txt) {
		deny all;
	}

        location / {
                #try_files $uri $uri/ =404;
                #Wordpress permalinks
                try_files $uri $uri/ /index.php?$args;
        }

        location ~ \.php$ {
                include fastcgi_params;
                fastcgi_pass unix:/run/php/php7.1-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;

                fastcgi_read_timeout 360s;
                fastcgi_buffer_size 128k;
                fastcgi_buffers 4 256k;

                #fastcgi_cache LAVEGANADEMADRID;
                #fastcgi_cache_valid 200 60m;
                #fastcgi_cache_bypass $no_cache;
                #fastcgi_no_cache $no_cache;

        }

        location ~ /.well-known {
            allow all;
        }

        location ^~ /.well-known/acme-challenge/ {
                allow all;
        }

	location ~* .(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
	  expires max;
	  log_not_found off;
	}
        
        location ~ /\.ht {
                deny all;
        }

        #fastcgi_cache start
        #set $no_cache 0;

        # POST requests and urls with a query string should always go to PHP
        #if ($request_method = POST) {
        #        set $no_cache 1;
        #}
        #if ($query_string != "") {
        #        set $no_cache 1;
        #}

        # Don't cache uris containing the following segments
	#if ($request_uri ~* "(/wp-admin/|/xmlrpc.php|/wp-(app|cron|login|register|mail).php|wp-.*.php|/feed/|index.php|wp-comments-popup.php|wp-links-opml.php|wp-locations.php|sitemap(_index)?.xml|[a-z0-9_-]+-sitemap([0-9]+)?.xml)") {
        #        set $no_cache 1;
        #}

        # Don't use the cache for logged in users or recent commenters
        #if ($http_cookie ~* "comment_author|wordpress_[a-f0-9]+|wp-postpass|wordpress_no_cache|wordpress_logged_in") {
        #        set $no_cache 1;
        #}

        # Don't cache if there is a cookie called PHPSESSID
        #if ($http_cookie = "PHPSESSID"){
        #    set $no_cache 1;
        #}

	#Protect WordPress from XML-RPC Attacks 
        location /xmlrpc.php {
                deny all;
        }

	#Only permision to GET, HEAD and POST
	if ($request_method !~ ^(GET|HEAD|POST)$) {
		return 444; 
	}
	
	location ~* (\.jpg|\.png|\.css)$ {
		if ($http_referer !~ ^($host) ) {
			return 500;
		}
	}
}
