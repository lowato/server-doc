server {
        listen 80;
	listen [::]:80;

	server_name  www.er-studio.es er-studio.es;
        return       301 https://er-studio.es$request_uri;
}

server {
	# SSL configuration

	listen 443 ssl http2;
	listen [::]:443 ssl http2;
	
        ssl_certificate /etc/letsencrypt/live/er-studio.es/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/er-studio.es/privkey.pem; # managed by Certbot
        ssl_dhparam /etc/ssl/certs/dhparam.pem;

        root /var/www/er-studio.es;
	index index.php;
	server_name er-studio.es www.er-studio.es;

	access_log /var/log/nginx/er-studio.es.access.log;
	error_log /var/log/nginx/er-studio.es.error.log;

	fastcgi_hide_header 'X-Powered-By';

	location = /favicon.ico {
		log_not_found off;
		access_log off;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
		access_log off;
	}

	location ~ /(\.|wp-config.php|readme.html|license.txt) {
		access_log off;
		log_not_found off;
		deny all;
	}

        location / {
                try_files $uri $uri/ /index.php?$args;
        }

        location ~ /.well-known {
            allow all;
        }

	location ^~ /.well-known/acme-challenge/ {
		allow all;
	}

	location ~* .(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
		expires max;
		log_not_found off;
		access_log off;
	}

	# Deny access to uploads that aren't images, videos, music, etc.
	location ~* ^/wp-content/uploads/.*.(html|htm|shtml|php|js|swf)$ {
	    deny all;
	}

	# Deny public access to wp-config.php
	location ~* wp-config.php {
	    deny all;
	}        

        location ~ /\.ht {
                deny all;
        }

        location ~ \.php$ {
                include fastcgi_params;
                fastcgi_pass unix:/run/php/php7.1-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		
		fastcgi_read_timeout 360s;
		fastcgi_buffer_size 128k;
		fastcgi_buffers 4 256k;                

        }

	add_header X-Cache $upstream_cache_status;

	#Protect WordPress from XML-RPC Attacks
	location /xmlrpc.php {
		deny all;
	}
	
	#Only permision to GET, HEAD and POST
	if ($request_method !~ ^(GET|HEAD|POST)$) {
		return 444; 
	}

	location ~* (\.jpg|\.png|\.css)$ {
		if ($http_referer !~ ^(https://er-studio.es) ) {
			return 500;
		}
	}

	ssl_certificate /etc/letsencrypt/live/er-studio.es/fullchain.pem; # managed by Certbot
	ssl_certificate_key /etc/letsencrypt/live/er-studio.es/privkey.pem; # managed by Certbot

	#Load custom marker img to Google Maps
	rewrite ^/wp-content/themes/Divi/images/marker.png$ https://er-studio.es/wp-content/themes/divi-child/images/marker.png redirect;
	rewrite ^/wp-content/themes/Divi/includes/builder/images/marker.png$ https://er-studio.es/wp-content/themes/divi-child/images/marker.png redirect;
}
