
# Servidor Optimizado para Wordpress sobre Debian 9

Mis notas de creación y gestión de servidor de Wordpress con Nginx como proxy inverso, Apache, php-fpm, MariaDB y phpMyAdmin.

Creado por [Pablo Lobato](https://www.linkedin.com/in/plobato/)</a>.

***

## Tabla de contenido

- **[Introducción](#Introducción)**
  - [Actualización de Debian](#Actualización-de-Debian)
  - [Gestión de usuarios](#Gestión-de-ususarios)
    - [Creación de usuario](#Creación-de-usuario)
  - [Instalación y configuración del cortafuegos UFW](#Instalación-y-configuración-del-cortafuegos-UFW)
  - [Configurar fecha y hora del servidor](#Configurar-fecha-y-hora-del-servidor)
    - [Cambiar ntpd por systemd-timesyncd](#Cambiar-ntpd-por-systemd-timesyncd)

- [Fail2Ban]()

- [Cloudflare CDN]()

- [Deshabilitar API Wordpress]()

***

## Introducción

Documentación para la creación y gestión de servidor de Wordpress con Nginx como Proxy Inverso, Apache, PHP, MariaDB, phpMyAdmin y UFW.

## Actualización de Debian

Antes de nada actualizamos el servidor Debian a la última versión:

```bash
apt update
apt upgrade

# Comprobamos que la versión de Debian es la última
lsb_release -a

#Nos mostrará algo así:
No LSB modules are available.
Distributor ID: Debian
Description:    Debian GNU/Linux 9.9 (stretch)
Release:        9.9
Codename:       stretch
```

## Gestión de usuarios

### Creación de usuario

Creamos un nuevo usuario:

```bash
USERNAME=NOMBRE_DE_USUARIO

adduser $USERNAME
```

Introducimos la contraseña y los datos que nos vaya solicitando (no son obligatorios). Es probable que una vez finalizado muestre el siguente mensaje:

```bash
sent invalidate(passwd) request, exiting
sent invalidate(group) request, exiting
sent invalidate(passwd) request, exiting
```

No es ningún problema, es solo un warning, pero si no vamos a usar sistemas como [LDAP](https://es.wikipedia.org/wiki/Protocolo_Ligero_de_Acceso_a_Directorios) podemos desinstalarlo y el mensaje desaparecerá. Para ello ejecutamos el siguiente comando:

```bash
apt remove unscd
```

Otorgamos prermisos de superadmin al usuario creado

```bash
usermod -aG sudo $USERNAME
```

## Instalación y configuración del cortafuegos UFW

Instalamos UFW

```bash
apt install ufw
```

Antes de activar UFW vamos a realizar unos cambios para mayor seguridad en el archivo :

```bash
nano /etc/ssh/sshd_config
```

- Cambiamos el puerto SSH:

    Decomentando la línea *#Port 22* y cambiando el puerto por el que nosotros deseemos.

- Bloqueamos el acceso del usuario root por SSH:

    Modificamos la línea *PermitRootLogin yes* por *PermitRootLogin no*

    ```bash
    #       $OpenBSD: sshd_config,v 1.100 2016/08/15 12:32:04 naddy Exp $

    # This is the sshd server system-wide configuration file.  See
    # sshd_config(5) for more information.

    # This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

    # The strategy used for options in the default sshd_config shipped with
    # OpenSSH is to specify options with their default value where
    # possible, but leave them commented.  Uncommented options override the
    # default value.

    Port 3303
    #AddressFamily any
    #ListenAddress 0.0.0.0
    #ListenAddress ::

    #HostKey /etc/ssh/ssh_host_rsa_key
    #HostKey /etc/ssh/ssh_host_ecdsa_key
    #HostKey /etc/ssh/ssh_host_ed25519_key

    # Ciphers and keying
    #RekeyLimit default none

    # Logging
    #SyslogFacility AUTH
    #LogLevel INFO

    # Authentication:

    #LoginGraceTime 2m
    PermitRootLogin no
    #StrictModes yes
    #MaxAuthTries 6
    #MaxSessions 10

    ...
    ```

Activamos al siguientes reglas para los servicios que vamos a utilizar:

```bash
#Listar reglas de UFW:
ufw app list

#Añadir regla SSH
#PORT: Puerto que hemos asignado en el archivo /etc/ssh/sshd_config
ufw allow PORT

#Añadir regla HTTP
ufw allow 80

#Añadir regla HTTPS
ufw allow 443

#Añadir regla SMTP
ufw allow 25

#Añadir regla MariaDB
ufw allow 3303

#Añadir regla 8080 Nginx Proxy - Apache
ufw allow 8080
```

Una vez activadas las reglas necesarias activamos el cortafuegos y reiniciamos SSH:

```bash
#Habilitar UFW
sudo ufw enable

#Reiniciar SSH
sudo systemctl reload sshd
```

Sin cerrar la session de SSH abierta, probamos a conectarnos con el nuevo usuario desde el nuevo puerto que hemos asisgnado parar comprobar que está todo correcto. En caso de que no podamos conectarnos todavía tenemos la sesión del *root* abierta y podremos realizar las modificacines oportunas para corregirlo.

## Configurar fecha y hora del servidor

Recurso externo:

- [How To Set Up Time Synchronization on Debian 9](https://www.digitalocean.com/community/tutorials/how-to-set-up-time-synchronization-on-debian-9)

```bash
#Fecha actual
date

#Cambiamos la hora
timedatectl set-timezone Europe/Madrid
```

### Cambiar ntpd por systemd-timesyncd

Primero eliminamos ntpd

```bash
apt purge ntp
```

A continuación activamos systemd-timesyncd

```bash
systemctl start systemd-timesyncd
```

Finalmente comprobamos que el sistema sigue funcionando correctamente:

```bash
systemctl status systemd-timesyncd
```

Para comprobar la zona horaria

```bash
timedatectl
```

## Swap

```bash
#Comprobar si Swap está activo
df -h

#Crear archivo Swap
fallocate -l 3G /swapfile

#Comprobamos que se ha creado correctamente
ls -lh /swapfile

#Habilitamos el archivo Swap
chmod 600 /swapfile

#Check
ls -lh /swapfile

#Marcamos el archivo como espacio de intercambio
mkswap /swapfile

#Habilitamos el archivo Swap
swapon /swapfile

#Check
swapon --show
free -h

#Hacemos que el archivo Swap sea permanente
cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab

#Comprbar y modificar valor de swappiness
cat /proc/sys/vm/swappiness
sysctl vm.swappiness=30

#Para que el cambio sea permanente
nano /etc/sysctl.conf
#Añadir al final del archivo:
vm.swappiness=30
#Ajusta el reparto de cache
vm.vfs_cache_pressure=50
```

## PHP-FPM

```bash
sudo apt install ca-certificates apt-transport-https 
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
echo "deb https://packages.sury.org/php/ stretch main" | sudo tee /etc/apt/sources.list.d/php.list
```

```bash
sudo apt-get update
sudo apt-get install php7.3-fpm php7.3-curl php7.3-gd php7.3-intl php7.3-mbstring php7.3-soap php7.3-xml php7.3-xmlrpc php7.3-zip
```

## MariaDB

Realizamos la instalación desde los repositorios de MariaDB:

```bash
# Agregamos los repositorios de MariaDB al sistema:
sudo apt-get install software-properties-common dirmngr
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://tedeco.fi.upm.es/mirror/mariadb/repo/10.3/debian stretch main'

#Una vez que se importa la clave y se agrega el repositorio, puede instalar MariaDB:
sudo apt-get update
sudo apt-get install mariadb-server

# Para una mejor seguridad de la instalación podemos ejecutar el siguiente scrpit:
sudo mysql_secure_installation
```
Si al acceder a mysql o durante el proceso del script se mostrara el siguiente mensaje:

```bash
mysql "ERROR 1524 (HY000): Plugin 'unix_socket' is not loaded"
```

Ejecutamos los siguientes comandos (más info: [mysql error 1524 - unix_socket](https://askubuntu.com/questions/705458/ubuntu-15-10-mysql-error-1524-unix-socket)):

```bash
sudo su

/etc/init.d/mysql stop
mysqld_safe --skip-grant-tables &
mysql -uroot

use mysql;
update user set password=PASSWORD("mynewpassword") where User='root';
update user set plugin="mysql_native_password";
quit;

/etc/init.d/mysql stop
kill -9 $(pgrep mysql)
/etc/init.d/mysql start

#Ya debería de poder acceder
mysql -u root -p

```


## Construción de paquete con Nginx y módulos

### Dependencias

Vamos a construir el paquete de Nginx junto a los modulos:

- PageSpeed
- Cache Purge
- GeoIP2
- Brolti

Y eliminar los módulos:

- autoindex_module
- memcached_module

los cuales vienen por defecto y no los necesito.

Antes de nada procedemos a instalar las dependencias necesarias:

```bash
sudo apt-get install build-essential zlib1g-dev libpcre3 libpcre3-dev unzip uuid-dev dpkg-dev git autoconf libtool automake
```

> Nota: El paquete *dpkg-dev* creo que no es necesario pero no estoy seguro.

- [PCRE](http://pcre.org/) – Soporte para expresiones regulares. Requerido para NGINX Core y el módulo Rewrite.

```bash
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.43.tar.gz
tar -zxf pcre-8.43.tar.gz
cd pcre-8.43
sudo ./configure
sudo make
sudo make install
```

- [zlib](http://www.zlib.net/) – Soporte para la compresión de cabeceras. Requerido para el módulo Gzip.

```bash
wget http://zlib.net/zlib-1.2.11.tar.gz
tar -zxf zlib-1.2.11.tar.gz
cd zlib-1.2.11
sudo ./configure
sudo make
sudo make install
```

- [OpenSSL](https://www.openssl.org/) – Soporte para el protocolo HTTPS. Requerido por el módulo SSL module y otros.

```bash
wget http://www.openssl.org/source/openssl-1.1.1b.tar.gz
tar -zxf openssl-1.1.1b.tar.gz
cd openssl-1.1.1b
sudo ./config --prefix=/usr/local/openssl --openssldir=/usr/local/openssl
sudo make
sudo make install
```

> Nota: Entiendo que en este punto ya puedo hacer limpieza de los paquetes descargados hasta ahora para las dependiencias, pero no lo he probado.

### Descarga y preparación de módulos

#### Brotli

Recurso externo:

- [Brotli](https://github.com/google/ngx_brotli)

```bash
sudo git clone https://github.com/google/ngx_brotli.git
sudo git submodule update --init --recursive
````

#### PageSpeed

Recurso externo:

- [PageSpeed](https://www.modpagespeed.com/doc/build_ngx_pagespeed_from_source)

Consuta la última versión disponible de [Pagespeed](https://www.modpagespeed.com/doc/release_notes)

```bash
NPS_VERSION=1.13.35.1-beta
cd modules
sudo wget https://github.com/apache/incubator-pagespeed-ngx/archive/v${NPS_VERSION}.zip
sudo unzip v${NPS_VERSION}.zip
nps_dir=$(find . -name "*pagespeed-ngx-${NPS_VERSION}" -type d)
cd "$nps_dir"
NPS_RELEASE_NUMBER=${NPS_VERSION/beta/}
psol_url=https://dl.google.com/dl/page-speed/psol/${NPS_RELEASE_NUMBER}.tar.gz
[ -e scripts/format_binary_url.sh ] && psol_url=$(scripts/format_binary_url.sh PSOL_BINARY_URL)
sudo wget ${psol_url}
sudo tar -xzvf $(basename ${psol_url})  # extracts to psol/
```

> Nota: En el momento de realizar la descarga de la última versión de pagespeed es la 1.13.35.2-stable (02/05/2019), pero esta me da problemas al descargar el psol. No lo encuentra para dicha versión. Así que he usado finalmente la versión 1.13.35.1-beta.

#### Cache Purge

```bash
sudo wget https://github.com/FRiCKLE/ngx_cache_purge/archive/master.zip
sudo unzip master.zip
sudo rm master.zip

```

#### GeoIP2

Añadimos el módulo [GeoIP2](https://github.com/leev/ngx_http_geoip2_module) en sustitución del módulo que viene por defecto: [http_geo_module](https://nginx.org/en/docs/http/ngx_http_geo_module.html). El cual quitamos cuando [configuremos el paquete](#Configuración-del-paquete-Nginx-personalizado) más adelante.

```bash
sudo git clone https://github.com/leev/ngx_http_geoip2_module.git
```
Una vez que había preparado todos los módulos y ejecuté el comando de configuración de Nginx con estos me mostró el siguiente mensaje:

```bash
checking for MaxmindDB library ... not found
./configure: error: the geoip2 module requires the maxminddb library.
```

Para solvernarlo hay que instalar la librería [libmaxminddb](https://github.com/maxmind/libmaxminddb), para lo que necestiamos las siguente librerías: `autoconf`, `libtool`, `automake`. Ya deberían de estar instaladas anteriormente, en caso de que falte alguna la instalamos:


```bash
sudo apt-get install autoconf libtool automake
```

A continuacion ya podremos descargar e instalar la librería [libmaxminddb](https://github.com/maxmind/libmaxminddb):

```bash
git clone --recursive https://github.com/maxmind/libmaxminddb
cd libmaxminddb
./bootstrap
./configure
make
sudo make install
```

#### Nginx

Recurso externo:

- [PageSpeed](https://www.modpagespeed.com/doc/build_ngx_pagespeed_from_source)

Consuta la última versión disponible de [Nginx](https://nginx.org/en/download.html):

```bash
NGINX_VERSION=1.16.0
cd
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
tar -xvzf nginx-${NGINX_VERSION}.tar.gz
cd nginx-${NGINX_VERSION}/
make
sudo make install
```

##### Eliminación de etiqueta Nginx y versión para mayor seguridad

Editamos el archivo [`src/http/ngx_http_header_filter_module.c`](nginx/src/http/ngx_http_header_filter_module.c) y cambiamos las líneas:

```c
static char ngx_http_server_string[] = "Server: nginx" CRLF;
static char ngx_http_server_full_string[] = "Server: " NGINX_VER CRLF;
```

por las siguientes:

```c
static char ngx_http_server_string[] = "Server: httpd" CRLF;
static char ngx_http_server_full_string[] = "Server: httpd" CRLF;
```

También podemos editar los html de los errores 4xx y 5xx desde el archivo [`src/http/ngx_http_special_response.c`](nginx/src/http/ngx_http_special_response.c) para que tampoco se muestren ahí la información del servidor. Podemos personalizar cada uno de ellos a nuestro gusto. Yo en mi caso lo único que he hecho es añadir el *meta* *viewport* para que se adapte a dispositivos móviles:

```html
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
```

#### Listado de módulos por defecto de Nginx

| Nombre del módulo  | Descripción |
| ------------- | ------------- |
| [http_access_module](https://nginx.org/en/docs/http/ngx_http_access_module.html)  | Acepta o deniega solicitudes de direcciones de clientes especificas.  |
| [http_auth_basic_module](https://nginx.org/en/docs/http/ngx_http_auth_basic_module.html)  | Limita el acceso a los recursos mediante la validación de nombre de usuario y contraseña mediante el protocolo de autenticación básica HTTP.  |
| [http_autoindex_module](https://nginx.org/en/docs/http/ngx_http_autoindex_module.html)  | Procesa las solicitudes que terminan con el carácter de barra diagonal (/) y produce una lista de directorios.  |
| [http_charset_module](https://nginx.org/en/docs/http/ngx_http_charset_module.html)  | `Content-Type`. Puede convertir datos de un conjunto de caracteres a otro.  |
| [http_empty_gif_module](https://nginx.org/en/docs/http/ngx_http_empty_gif_module.html)  | Emite un GIF transparente de un solo píxel.  |
| [http_fastcgi_module](https://nginx.org/en/docs/http/ngx_http_fastcgi_module.html)  | Pasa las solicitudes al servidor FastCGI.  |
| [http_geo_module](https://nginx.org/en/docs/http/ngx_http_geo_module.html)  | Crea variables con valores que dependen de la dirección IP del cliente.  |
| [http_gzip_module](https://nginx.org/en/docs/http/ngx_http_gzip_module.html)  | Comprime las respuestas usando *gzip*, reduciendo la cantidad de datos transmitidos a la mitad o más.  |
| [http_limit_conn_module](https://nginx.org/en/docs/http/ngx_http_limit_conn_module.html)  | Limita el número de conexiones por clave definida, en particular, el número de conexiones desde una única dirección IP.  |
| [http_limit_req_module](https://nginx.org/en/docs/http/ngx_http_limit_req_module.html)  | Limita la tasa de procesamiento de solicitudes por una clave definida, en particular, la tasa de procesamiento de solicitudes provenientes de una única dirección IP.  |
| [http_map_module](https://nginx.org/en/docs/http/ngx_http_map_module.html)  | Crea variables cuyos valores dependen de los valores de otras variables.  |
| [http_memcached_module](https://nginx.org/en/docs/http/ngx_http_memcached_module.html)  | asa las solicitudes a un servidor memcached.  |
| [http_proxy_module](https://nginx.org/en/docs/http/ngx_http_proxy_module.html)  | Pasa las solicitudes HTTP a otro servidor.  |
| [http_referer_module](https://nginx.org/en/docs/http/ngx_http_referer_module.html)  | Bloquea las solicitudes con valores no válidos del encabezado *Referer*. |
| [http_rewrite_module](https://nginx.org/en/docs/http/ngx_http_rewrite_module.html)  | Cambia la URI de solicitud utilizando expresiones regulares y devuelve redirecciones; Condicionalmente selecciona configuraciones. Requiere la librería [PCRE](http://pcre.org/).  |
| [http_scgi_module](https://nginx.org/en/docs/http/ngx_http_scgi_module.html)  | Pasa las solicitudes a un servidor SCGI.  |
| [http_ssi_module](https://nginx.org/en/docs/http/ngx_http_ssi_module.html)  | Procesa los comandos SSI (Server Side Include) en las respuestas que pasan a través de él.  |
| [http_split_clients_module](https://nginx.org/en/docs/http/ngx_http_split_clients_module.html)  | Crea variables adecuadas para los test A/B.  |
| [http_upstream_hash_module](https://nginx.org/en/docs/http/ngx_http_upstream_module.html#hash)  | Habilita el método genérico Hash load-balancing.  |
| [http_upstream_ip_hash_module](https://nginx.org/en/docs/http/ngx_http_upstream_module.html#ip_hash)  | Habilita el método IP Hash load-balancing.  |
| [http_upstream_keepalive_module](https://nginx.org/en/docs/http/ngx_http_upstream_module.html#keepalive)  | Habilita conexiones keepalive.  |
| [http_upstream_least_conn_module](https://nginx.org/en/docs/http/ngx_http_upstream_module.html#least_conn)  | Habilita el método Least Connections load-balancing.  |
| [http_upstream_zone_module](https://nginx.org/en/docs/http/ngx_http_upstream_module.html#zone)  | Habilita zonas de memoria compartida.  |
| [http_userid_module](https://nginx.org/en/docs/http/ngx_http_userid_module.html)  | Establece cookies adecuadas para la identificación del cliente.  |
| [http_uwsgi_module](https://nginx.org/en/docs/http/ngx_http_uwsgi_module.html)  | Pasa las solicitudes a un servidor uwsgi.  |

#### Configuración del paquete Nginx personalizado


```bash
cd nginx-1.16.0
```

```bash
sudo ./configure --prefix=/etc/nginx \
--sbin-path=/usr/sbin/nginx \
--modules-path=/usr/lib/nginx/modules \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock \
--user=www-data \
--group=www-data \
--build=Debian \
--with-pcre=/home/lobato/pcre-8.43/ \
--with-zlib=/home/lobato/zlib-1.2.11 \
--with-http_ssl_module \
--with-http_v2_module \
--add-module=/home/lobato/modules/ngx_brotli \
--add-module=/home/lobato/modules/ngx_cache_purge-master \
--add-module=/home/lobato/modules/incubator-pagespeed-ngx-1.13.35.1-beta \
--add-dynamic-module=/home/lobato/modules/ngx_http_geoip2_module \
--without-http_autoindex_module \
--without-http_empty_gif_module \
--without-http_geo_module \
--without-http_memcached_module \
--without-http_scgi_module \
--without-http_ssi_module \
--without-http_split_clients_module \
--without-http_uwsgi_module \
;
```

```bash
sudo make
sudo make install

# Comprobamos que Nginx se ha instalado
sudo nginx -V

# Comprobamos que la configuración no tiene errores
sudo nginx -t
```

Ahora vamos a crear un archivo de unidad systemd para Nginx:

```bash
sudo nano /etc/systemd/system/nginx.service
```

Añadimos en el el siguiente código y guardamos:

```bash
[Unit]
Description=nginx - high performance web server
Documentation=https://nginx.org/en/docs/
After=network-online.target remote-fs.target nss-lookup.target
Wants=network-online.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -c /etc/nginx/nginx.conf
ExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID

[Install]
WantedBy=multi-user.target
```

Habilitamos NGINX para que se inicie en el arranque del sistema:

```bash
sudo systemctl enable nginx.service
```

Iniciamos NGINX:

```bash
sudo systemctl start nginx.service
```

Creamos las rutas que faltan para realizar la configuración de Nginx:

```bash
sudo mkdir /etc/nginx/{conf.d,snippets,sites-available,sites-enabled}
```

Estos son los ficheros en la ruta por defecto de la compilación:

- `/usr/local/nginx/` - Directorio de configuración
- `/usr/local/nginx/conf.d/` - Directorio de configuración de virtualhost y SSL
- `/var/log/nginx/` - Directorio de logs
- `/usr/share/nginx/html` - Directorio raíz
- `/usr/local/nginx/nginx.conf` - Fichero de configuración
- `/var/log/nginx/access.log` - Fichero de log de accesos
- `/var/log/nginx/error.log` - Fichero de log de error

#### Configurar Nginx para usar con PHP-FPM

```bash
sudo mkdir /var/www/your_domain

sudo nano /etc/nginx/sites-available/your_domain
```

```nginx
server {
    listen 80;
    listen [::]:80;

    root /var/www/your_domain;
    index index.php index.html index.htm;

    server_name your_domain;

    charset UTF-8;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
    }

}
```

#### Wordpress

##### Crear base de datos

##### Descarga de WP y configuración de archivos

```bash
cd /var/www/
sudo wget https://es.wordpress.org/latest-es_ES.tar.gz
sudo tar -zxvf latest-es_ES.tar.gz
sudo chown www-data:www-data  -R wordpress/
sudo mv wordpress/ your_domain
```

Cuando configuras WP, el servidor web puede necesitar acceso de escritura a los archivos. Así que cambiamos los permisos.

```bash
cd your_domain
sudo chown www-data:www-data  -R * # Let Apache be owner
sudo find . -type d -exec chmod 755 {} \;  # Change directory permissions rwxr-xr-x
sudo find . -type f -exec chmod 644 {} \;  # Change file permissions rw-r--r--
```

Después de la configuración, se deben ajustar los permisos; de acuerdo con Hardening WordPress, todos los archivos, excepto `wp-content`, deben tener permisos de escritura solo para tu cuenta de usuario. `wp-content` debe tener permisos de escritura también para www-data.

```bash
cd your_domain
sudo chown <username>:<username>  -R * # Let your useraccount be owner
sudo chown www-data:www-data wp-content # Let apache be owner of wp-content
Maybe you want to change the contents in wp-content later on. In this case you could
```

> Nota: Comando para actualizar la url de un proyecto de WordPress:

```sql
update wp_options set option_value = 'your_domain' where option_id = 1 or option_id = 2;
```

##### Instalación